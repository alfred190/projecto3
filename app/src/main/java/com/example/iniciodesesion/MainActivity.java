package com.example.iniciodesesion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.core.DatabaseInfo;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private EditText nombre;
    private EditText correo;
    private EditText clave;
    private Button registrar;

    //VARIABLES DE DATOS A REGISTRAR

    private String nombres = "";
    private String email = "";
    private String contraseña = "";

    FirebaseAuth mAuth;
    DatabaseReference bd_registro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        bd_registro = FirebaseDatabase.getInstance().getReference();

        nombre = (EditText) findViewById(R.id.id_nombre);
        correo = (EditText) findViewById(R.id.id_correo);
        clave = (EditText) findViewById(R.id.id_clave);
        registrar = (Button) findViewById(R.id.btn_registrar);

        registrar.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                nombres = nombre.getText().toString();
                email = correo.getText().toString();
                contraseña = clave.getText().toString();

                if (!nombres.isEmpty() && !email.isEmpty() && !contraseña.isEmpty()) {

                    if (contraseña.length() >= 6) {
                        registrarUsuario();
                    } else {
                        Toast.makeText(MainActivity.this, "La contraseña debe tener mas de 5 caracteres", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Se deben completan todos los campos de texto", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void registrarUsuario() {

        mAuth.createUserWithEmailAndPassword(email, contraseña).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    Map<String,Object> map = new HashMap<>();
                    map.put("nombres",nombres);
                    map.put("email",email);
                    map.put("contraseña",contraseña);

                    String id = mAuth.getCurrentUser().getUid();

                    bd_registro.child("Usuario").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {

                        @Override
                        public void onComplete(@NonNull Task<Void> task2) {
                            if (task2.isSuccessful()) {
                                startActivity(new Intent(MainActivity.this, mostrar.class));
                                finish();
                            }
                            else {
                                Toast.makeText(MainActivity.this,"No se pudieron crear los datos correctamente", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(MainActivity.this, "No se pudo crear el usuario", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}